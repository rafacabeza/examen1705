<?php

use Illuminate\Database\Seeder;

class PizzasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // factory(App\Pizza::class, 20)->create();


        for ($i=1; $i < 10; $i++) {
            $pizza = factory(App\Pizza::class)->create();

            $ingredient = App\Ingredient::where('type_id', 1)->get()->random();
            $pizza->ingredients()->attach($ingredient->id);
            $ingredient = App\Ingredient::where('type_id', 2)->get()->random();
            $pizza->ingredients()->attach($ingredient->id);
            $ingredients = App\Ingredient::where('type_id', '>', 2)->get()->random(rand(3, 5));
            foreach ($ingredients as $ingredient) {
                $pizza->ingredients()->attach($ingredient->id);
            }
        }
    }
}
